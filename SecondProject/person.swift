//
//  person.swift
//  SecondProject
//
//  Created by Luis Oliveira on 29/09/2016.
//  Copyright © 2016 mcmlf. All rights reserved.
//

import Foundation

class Person {
    var firstName:String?
    var lastName:String?
    var nationality:String
    
    init() {
        self.nationality = "Portuguese"
    }
    
    init(firstName:String, lastName:String, nationality:String) {
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
    }
    
    func fullName() -> String {
        return firstName! + " " + lastName! + "Teste"
    }
}
