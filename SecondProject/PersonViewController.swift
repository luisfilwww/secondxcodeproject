//
//  PersonViewController.swift
//  SecondProject
//
//  Created by Luis Oliveira on 29/09/2016.
//  Copyright © 2016 mcmlf. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController {

    @IBOutlet weak var labelText: UILabel!
    
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var nationalityText: UITextField!
    
    var person = Person()
    
    @IBAction func setButton(_ sender: AnyObject) {
        person.firstName = firstNameText.text!
        person.lastName = lastNameText.text!
        if let nat = nationalityText.text {
            person.nationality = nat
        }
        
        labelText.text = person.fullName()
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
